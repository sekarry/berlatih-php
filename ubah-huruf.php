<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>
<body>
<?php
    function ubah_huruf($string){
        $string_baru = "";
        for($i = 0; $i < strlen($string); $i++) {
            $huruf = $string[$i];
            $huruf++;
            $string_baru .= $huruf;
        }

        return $string_baru;
        }
        
        // TEST CASES
        echo ubah_huruf('wow') ."<br>"; // xpx
        echo ubah_huruf('developer') ."<br>"; // efwfmpqfs
        echo ubah_huruf('laravel') ."<br>"; // mbsbwfm
        echo ubah_huruf('keren') ."<br>"; // lfsfo
        echo ubah_huruf('semangat') ."<br>"; // tfnbohbu
?>
</body>
</html>