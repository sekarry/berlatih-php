<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>
<body>
<?php
    function tukar_besar_kecil($string){
        $string_baru = " ";
        for ($i = 0; $i < strlen($string); $i++) {
            $huruf = $string[$i];
            if ($huruf == strtolower($huruf)) {
                $huruf = strtoupper($huruf);
            } else {
                $huruf = strtolower($huruf);
            }

            $string_baru .= $huruf;
        }

        return $string_baru;

        }
        
        // TEST CASES
        echo tukar_besar_kecil('Hello World') ."<br>"; // "hELLO wORLD"
        echo tukar_besar_kecil('I aM aLAY') ."<br>"; // "i Am Alay"
        echo tukar_besar_kecil('My Name is Bond!!') ."<br>"; // "mY nAME IS bOND!!"
        echo tukar_besar_kecil('IT sHOULD bE me') ."<br>"; // "it Should Be ME"
        echo tukar_besar_kecil('001-A-3-5TrdYW') ."<br>"; // "001-a-3-5tRDyw"
?>
</body>
</html>